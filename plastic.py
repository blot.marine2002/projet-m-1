#imports standards
import math
import random
import string
import copy
import numpy as np
from collections import defaultdict
from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import minimum_spanning_tree
import pandas as pd

#imports de mesa
import mesa
import mesa.space
from mesa import Agent, Model, DataCollector
from mesa.time import RandomActivation
from mesa.visualization import ModularVisualization
from mesa.visualization.ModularVisualization import VisualizationElement, ModularServer
from mesa.visualization.modules import ChartModule

#import de PADE pour les messages
from pade.acl.messages import ACLMessage  # Vous trouverez dans cette classe tous les performatifs

mailboxes = {}  # Les boites aux lettres des agents qui leurs permettent de gérer les messages. Variable globale

class Message:
    def __init__(self, act: string, content, sender, receivers):
        self.act = act  # act doit etre un acte de langage pris dans ACLMessage
        self.content = content  # contenu. Pour le moment, peut etre n'importe quel type
        self.sender = sender  # l'agent ayant envoyé le message
        self.receivers = receivers  # liste des agents destinataires. Doit etre sous la forme d'une liste,
                                    # meme d'un unique agent

    def send(self):
        """
        Méthode d'envoi du message
        :return: None
        """

        for a in self.receivers:
            mailboxes[a].append(self)

def calcul_kilo(model):
    usv=model.usvs
    kilometre=0
    for bateau in usv:
        valeur=bateau.getkilo()
        kilometre+= valeur
    return kilometre

def max_step(model):
    usv = model.usvs
    step_max=0
    for bateau in usv:
        if bateau.step_total>step_max:
            step_max=bateau.step_total

    return step_max



class RecuperationPlastique(mesa.Model):

    def distance(self, usv1, usv2):
        return math.sqrt((usv1.x - usv2.x) ** 2 + (usv1.y - usv2.y) ** 2)

    def neighbours(self):
        for usv in self.usvs:
            usv.neighbours = []
        distances = [[self.distance(self.usvs[i], self.usvs[j]) for j in range(len(self.usvs))]
                                for i in range(len(self.usvs))]
        distances_matrix = csr_matrix(distances)
        Tcsr = minimum_spanning_tree(distances_matrix)
        connection = np.array(distances) < self.distance_max_communication
        for i in range(len(self.usvs)):
            for j in range(len(self.usvs)):
                if i != j and (Tcsr[i, j] != 0 or connection[i, j]):
                    if self.usvs[j] not in self.usvs[i].neighbours:
                        self.usvs[i].neighbours.append(self.usvs[j])
                    if self.usvs[i] not in self.usvs[j].neighbours:
                        self.usvs[j].neighbours.append(self.usvs[i])

    def __init__(self, n_dechets, n_usv, distance_max_communication):
        mesa.Model.__init__(self)
        self.running = True
        self.distance_max_communication = distance_max_communication
        self.space = mesa.space.ContinuousSpace(600, 600, False)  # Espace utilisé (600*600px)
        # scheduler, active les agents. Ici, le random scheduler les active de manière aléatoire à chaque step
        self.schedule = RandomActivation(self)
        self.usvs = []  # Liste des USVs
        self.dechets = []  # Liste des déchets à tenir à jour. Utilisé pour la courbe en bas de l'écran
        for i in range(n_dechets):  # Creation des déchets
            dechet = Dechet(i, self, random.randint(0, 600), random.randint(0, 600))
            self.dechets.append(dechet)
            self.schedule.add(dechet)  # ajouté au scheduler
        for i in range(n_usv):  # création des USV
            bateau = USV(i + n_dechets, self, random.randint(0, 600), random.randint(0, 600), 15, self.dechets,n_usv)
            self.usvs.append(bateau)
            self.schedule.add(bateau)


    # Création du datacollector. Collecte des données pour la courbe en bas de l'écran.
    # Servira aussi pour la collecte de données en batch
        self.datacollector = DataCollector(
            model_reporters={"dechets": lambda model: len(model.dechets), "kilometre": calcul_kilo,"nombre de step": max_step
                             # "Delivered": lambda model: model.computed_items_nb
                             },
            agent_reporters={})



    def study(self, dictionnaire):
        cmp=0
        for keys in dictionnaire.keys():
            for cle in dictionnaire.keys():
                if dictionnaire[keys]==dictionnaire[cle]:
                    cmp+=1
            if cmp!=1:
                return 1
            cmp = 0

        return 0

    def step(self):  # Fonction activée par le modèle à chaque tour
        self.neighbours()  # on détermine les voisins de chaque agent
        self.schedule.step()  # On active les agents à travers le scheduler qui appelle la méthode step des agents
        self.datacollector.collect(self)  # On recalcule les données
        if self.schedule.steps >= 1000 or len(self.dechets) == 0:
            self.running = False  # S'il n'y a plus de déchets ou si le nombre de tours est supérieur à 300, on s'arrête


class CommunicatingAgent(Agent):  # classe héritée d'Agent qui gère les messages
    # (si jamais on voulait ajouter autre chose que des USV par exemple)
    def __init__(self, unique_id: int, model: Model):
        super().__init__(unique_id, model)#le super va permettre d'hériter de la classe parent Agent
        mailboxes[self] = []  # on crée une boite aux lettres pour l'agent

    def send(self, msg: Message):
        """
        Fonction d'envoi d'un message. Appelle simplement la méthode d'envoi du message
        :param Message msg: le message à envoyer
        :return: None
        """
        msg.send()

    def receive(self):
        """
        Lors de la réception, on renvoie la liste des messages et on la vide
        :return: la liste des messages reçus
        """
        messages = mailboxes[self]
        mailboxes[self] = []
        return messages


class USV(CommunicatingAgent):  # L'agent USV
    def __init__(self, unique_id: int, model: RecuperationPlastique,
                 x, y, max_speed: float, dechet, n_usv):
        super().__init__(unique_id, model)
        self.x = x  # position en x
        self.y = y  # position en y
        self.max_speed = max_speed  # vitesse max pour le déplacement
        self.destination = None  # destination (déchet qui doit être récupérer par l'USV)
        self.enchere_precedente = None
        self.neighbours = []
        self.dechet = dechet
        self.nbr_voulue = max([len(self.dechet),n_usv]) * n_usv   #calcul du nombre minimal d'itérations de step pour être sur que chaque robot est finit de calculer sont tableau
        self.tableau = {d: (0, -1)for d in dechet}
        self.dechet_CBBA= {}
        self.compteur_dechet = 0 #compteur permettant de savoir quel est le numéro de la clé de dechet_CBBA à utiliser
        self.kilometre= 0
        self.step_total=0
        self.dechet_retenues = {}   #Dechet que le robot va ramasser pour le moment : {Dechet : enchère}
        class Position_USV:
            def __init__(pos):
                pos.x = self.x
                pos.y = self.y

        self.position_usv = Position_USV()
    def goto(self, destination):
        """
        Établit la destination de l'agent
        :param destination: doit avoir un x et un y, la destination de l'agent
        :return: None
        """
        self.destination = destination

    def move(self):
        """
        Se déplace à vitesse maximale vers la destination; appelle move_to
        :return:
        """
        self.move_to(self.destination, self.max_speed)

    def move_to(self, dest, speed):
        """
        Se déplace à une vitesse déterminée vers une destination déterminée
        :param dest: destination. Doit avoir un x et un y
        :param float speed: vitesse
        :return: None
        """
        movement = tuple(min(
            (speed * (dest.x - self.x) / np.linalg.norm((dest.x - self.x, dest.y - self.y)), speed * (dest.y - self.y) /
             np.linalg.norm((dest.x - self.x, dest.y - self.y))),
            (dest.x - self.x, dest.y - self.y), key=lambda p: np.linalg.norm(p)))
        self.x += movement[0]
        self.y += movement[1]



####################################################################################################################################################
#                                                                                                                                                  #
#                                                               Début CBBA                                                                         #
#                                                                                                                                                  #
####################################################################################################################################################

    def step(self):
        #On traite les données du dictionnaire reçu
        recu = self.receive()#reception de la liste de message
        tableaux_voisin=[m.content for m in recu]

        # première phase : on traite les données reçues
        for t in tableaux_voisin:
            for key in t:
                if t[key][0] > self.tableau[key][0]:  # si enchère voisin supérieur à celle présente dans notre tableau, alors on remplace l'enchère
                    self.tableau[key] = t[key]
                elif t[key][0] == self.tableau[key][0] and t[key][1] < self.tableau[key][1]:  # pour gérer les cas d'égalités sur les enchères. l'ID le plus petit remporte
                    self.tableau[key] = t[key]

        # puis on traite les enchères de notre bateau
        if self.model.schedule.steps <= self.nbr_voulue:
            self.faire_encheres()  # On peut faire les enchères
            message = Message(ACLMessage.PROPOSE, self.tableau, self, self.neighbours)  # création du message à envoyer auxautres USV (type : proposition, message : tableau, sender : self, reveiver : neighbours)
            self.send(message)  # envoie du message avec le tableau


        elif self.model.schedule.steps == self.nbr_voulue + 1:
            self.dechet_CBBA = {}  # liste de déchets attribués à l'USV
            self.step_avant_deplacement = self.model.schedule.steps
            #print(self.step_avant_deplacement)
            for key, attribut in self.tableau.items():
                if attribut[1] == self.unique_id:
                    self.dechet_CBBA[key] = attribut[0]  # Stocke tout les déchets du tableau lié à l'USV dans la liste_dechet_attribue

            ls_dechet = sorted(self.dechet_CBBA.items(), key=lambda t: t[1], reverse=True)  # trie de la liste de déchet
            self.dechet_CBBA = {}
            for dechet in ls_dechet:
                self.dechet_CBBA[dechet[0]] = dechet[1]  # dictionnaire trié
            if self.enchere_precedente is not None:
                self.kilometre=1/self.enchere_precedente
                #print("Le robot ", self.unique_id, " va se déplacer de ",self.kilometre )


       # LES ENCHERES SONT FINIT, ON DEPLACE LES USV

        else:
            if self.destination is None and self.dechet_CBBA != {}:
                key0 = [key for key in self.dechet_CBBA.keys()][0]
                self.goto(key0)

            try:
                if (self.x, self.y) != (self.destination.x, self.destination.y) and self.destination is not None:   #Si on est pas sur le déchet, on s'y rend
                    self.move()

                else:  # s'il est à sa destination, supprime le plastique en reçoit puisque tous les agents le lui envoient

                    self.model.schedule.remove(self.destination)
                    self.model.dechets.remove(self.destination)
                    self.compteur_dechet += 1

                    try:
                        self.goto([key for key in self.dechet_CBBA.keys()][self.compteur_dechet])   #on affecte le déchet suivant à self.destination
                    except:
                        self.destination=None   #Si il n'y a plus de déchet à affecter, alors on place self.destination à None
                        self.dechet_CBBA = {}
                        self.step_total = self.model.schedule.steps - self.step_avant_deplacement
                        #print("le bateau", self.unique_id, "va chercher les déchets en", self.step_total, "step")



            except:
                pass

    def faire_encheres(self):

        self.verif_presence()
        self.remplacement_tableau()

    def verif_presence(self):
        if self.unique_id == 22:
            pass
        self.dict_dechet_now = {}
        for key, attribut in self.tableau.items():   #Boucle for pour récupérer tout les déchet associé à l'USV
            if attribut[1] == self.unique_id:
                self.dict_dechet_now[key] = attribut[0]   #récupérer les déchets associés à l'USV dans le tableau
        self.dict_dechet_now = self.trie_dictionnaire(self.dict_dechet_now)   #On trie le dictionnaire

        if self.dechet_retenues != self.dict_dechet_now:     #self.dechet_retenus est déjà trié
            try:
                compteur = 0
                ls_now = [cle for cle in self.dict_dechet_now.keys()]
                ls_retenus = [cle for cle in self.dechet_retenues.keys()]
                while ls_retenus[compteur] == ls_now[compteur]: #on cherche dans la première différance entre les deux listes
                    compteur += 1

                trop = compteur - len(self.dechet_retenues)
                dechet_enlever = list(self.dechet_retenues.keys())[trop:] #On recupère les clés des dechets à enlever de notre liste
                for dechet in dechet_enlever:
                    del self.dechet_retenues[dechet]        #Enlève le premier déchet n'étant pas associé au robot et tout les suivants
                    if self.tableau[dechet][1] == self.unique_id:   #changer uniquement les enchères des déchets enchèrie par cet USV
                        self.tableau[dechet] = (0,-1)

            except:
                taille = len(self.dict_dechet_now) - len(self.dechet_retenues)
                self.dechet_retenues = self.dict_dechet_now  # si tout les déchets sont les mêmes jusqu'à la fin de dict_dechet_now, mais que self.dechet_retenues est plus long, alors on ne garde que les déchets du premier dict
                dechet_enlever = list(self.dechet_retenues.keys())[taille:]
                for dechet in dechet_enlever:
                    if self.tableau[dechet][1] == self.unique_id:
                        self.tableau[dechet] = (0, -1)



       

    def remplacement_tableau(self):
        if self.unique_id == 22:
            pass
        compteur = 0

        copie_dechet = self.dechet.copy()   #On récupère tout les déchets
        dict_a_tester = {self.position_usv : math.inf}   #Valeur d'enchère infini puisque le robot ne bouge pas
        dict_a_tester.update(self.dechet_retenues)   #On créer un dictionnaire avec la position initiale de l'USV en première valeur puis les déchets assignés à l'USV
        list_dict_a_tester = list(dict_a_tester.keys())

        for dechet in dict_a_tester:   #On parcours les clés du dictionnaire
            encheres = {}
            try:    #On fait un try car le premier "dechet" est l'USV qui n'est donc pas compris dans copie_dechet
                copie_dechet.remove(dechet)
            except:
                pass

            for d in copie_dechet:   #On parcours la liste des déchets restants
                min = self.calcul_enchere_CBBA(dechet.x, d.x, dechet.y, d.y, dict_a_tester[dechet])   #[dechet actuelle, dechet suivant, enchère du déchet actuelle]
                encheres[d] = min  # on remplie le dictionnaire avec le nom du déchet et son enchère
            encheres = self.trie_dictionnaire(encheres)

            try :   #On fait un try jusqu'à arriver à la fin de la list_dict_a_tester, car on fait compteur +1
                dechet_suivant = list_dict_a_tester[compteur+1]
                stop = 0
                for d in encheres:
                    if stop == 0:
                        if d == dechet_suivant:
                            stop = 1
                        elif encheres[d] > self.tableau[d][0]:
                            stop = 1
                            ind = list_dict_a_tester.index(dechet)  # On recup tout les dechets a remplacer
                            lsReset = list_dict_a_tester[ind + 1:]
                            for supprime in lsReset:
                                del self.dechet_retenues[supprime]  # On enlève l'assignation des dechets au robot
                                self.tableau[supprime] = (0, -1)  # On met leur enchère à 0 dans le tableau et l'USV à -1
                                self.enchere_precedente = encheres[d]
                        else:
                            pass

            except:   #Si tout les déchets assignés sont encore les meilleurs possible
                test = 0
                for dechet_USV in encheres:  # ls_encheres est trié dans l'ordre de la plus grande vers la plus petite enchère
                    if test == 0:
                        for dechet_tableau in self.tableau:
                            if dechet_tableau == dechet_USV:  # On cherche à comparer le même déchet entre ls_encheres et self.tableau
                                gain_marginal = encheres[dechet_USV] - self.tableau[dechet_tableau][0]  # on regarde si c'est rentable de changer l'USV
                                if gain_marginal > 0:  # si sa valeur marginal est strictement plus grande que 0, alors il a une meilleur enchère
                                    self.enchere_precedente = encheres[dechet_USV]
                                    self.tableau[dechet_USV] = (self.enchere_precedente, self.unique_id)  # et le déchet associé à ce gain marginal
                                    self.dechet_retenues[dechet_USV] = self.enchere_precedente
                                    self.dechet_retenues = self.trie_dictionnaire(self.dechet_retenues)
                                    test = 1
            compteur += 1

    def calcul_enchere_CBBA(self, X_position_bateau, X_position_dechet, Y_position_bateau, Y_position_dechet,enchere_precedente):
        try:
            return 1 / ((1 / enchere_precedente) + math.sqrt((X_position_bateau - X_position_dechet) ** 2 + (Y_position_bateau - Y_position_dechet) ** 2))

        except:
            try:
                return 1 / math.sqrt((X_position_bateau - X_position_dechet) ** 2 + (Y_position_bateau - Y_position_dechet) ** 2)

            except:
                return 0

    def trie_dictionnaire(self, dictionnaire):
        ls_dechet = sorted(dictionnaire.items(), key=lambda t: t[1],reverse=True)  # trie de la liste de déchet du plus grand au plus petit des valeurs
        dictionnaire = {}
        for dechet in ls_dechet:
            dictionnaire[dechet[0]] = dechet[1]  # dictionnaire trié
        return dictionnaire

    def portrayal_method(self):
        """
        Méthode d'affichage utilisé par le js
        :return: l'objet à interpréter par le javascript
        """
        portrayal = {"Shape": "arrowHead", "s": 1, "Filled": "true", "Color": "DimGrey", "Layer": 2, 'x': self.x,
                     'y': self.y}
        if self.destination and not (self.destination.x == self.x and self.destination.y == self.y):
            if self.destination.y - self.y > 0:
                portrayal['angle'] = math.acos((self.destination.x - self.x) /
                                                   np.linalg.norm(
                                                       (self.destination.x - self.x, self.destination.y - self.y)))
            else:
                portrayal['angle'] = 2 * math.pi - math.acos((self.destination.x - self.x) /
                                                                 np.linalg.norm(
                                                                     (self.destination.x - self.x,
                                                                      self.destination.y - self.y)))
        else:
            portrayal['angle'] = 0

        return portrayal

    def getkilo(self):
        return self.kilometre

    def getStep(self):
        return self.step_total


class Dechet(Agent):
    def __init__(self, unique_id: int, model, x, y):
        super().__init__(unique_id, model)
        self.x = x  # position en x
        self.y = y  # position en y

    def step(self):  # Pour le moment, les déchets ne bougent pas
        pass

    @staticmethod
    def portrayal_method():
        color = "GreenYellow"
        r = 6
        portrayal = {"Shape": "circle",
                     "Filled": "true",
                     "Layer": 1,
                     "Color": color,
                     "r": r}
        return portrayal


class ContinuousCanvas(VisualizationElement):
    #  Les includes utilisés pour l'affichage dans le navigateur
    local_includes = [
        "./js/simple_continuous_canvas.js",  # le js pour l'affichage principal
        "./js/jquery.min.js",  # jquery, également nécessaire pour l'affichage
    ]

    def __init__(self, canvas_height=500,
                 canvas_width=500, instantiate=True):
        # Visualisation
        VisualizationElement.__init__(self)
        self.canvas_height = canvas_height
        self.canvas_width = canvas_width
        self.identifier = "sea-canvas"
        if instantiate:
            new_element = ("new Simple_Continuous_Module({}, {},'{}')".
                           format(self.canvas_width, self.canvas_height, self.identifier))
            self.js_code = "elements.push(" + new_element + ");"

    @staticmethod
    def portrayal_method(obj):
        """
        Pour afficher un élément, on appelle sa fonction portrayal_method
        :param obj: l'objet à afficher
        :return: None
        """
        return obj.portrayal_method()

    def render(self, model):
        """
        Rendu du model lui-meme
        :param model: le PlasticModel
        :return: l'objet à interpréter par javascript
        """
        representation = defaultdict(list)
        for obj in model.schedule.agents:
            portrayal = self.portrayal_method(obj)
            if portrayal:
                portrayal["x"] = ((obj.x - model.space.x_min) /
                                  (model.space.x_max - model.space.x_min))
                portrayal["y"] = ((obj.y - model.space.y_min) /
                                  (model.space.y_max - model.space.y_min))
                representation[portrayal["Layer"]].append(portrayal)
        return representation


def run_single_server():
    """
    Fonction principale d'appel. C'est ici qu'on définira si on affiche ou si on fait un batch
    :return:
    """
    chart = ChartModule([{"Label": "dechets",  # la courbe
                          "Color": "Red"},
                         # {"Label": "Delivered",
                         #  "Color": "Blue"}
                         ],
                        data_collector_name='datacollector')

    server = ModularServer(RecuperationPlastique,  # L'ensemble des éléments.
                           # Ici, on a un ModularServer avec deux éléments, la fenêtre principale et la courbe
                           [ContinuousCanvas(), chart],
                           # [ContinuousCanvas()],
                           "Recolte de dechets plastiques en mer",
                           # Les sliders qui permettent de définir le nombre de déchets/d'USV
                           {"n_dechets": ModularVisualization.UserSettableParameter('slider',
                                                                                    "Nombre de dechets",
                                                                                    10, 3, 20, 1),
                            "n_usv": ModularVisualization.UserSettableParameter('slider',
                                                                                  "Nombre d'USV",
                                                                                  15, 3, 30, 1),
                            "distance_max_communication": ModularVisualization.UserSettableParameter('slider',
                                                                                "Distance de communication",
                                                                                 0, 0, 850, 20)})
    server.port = 8521  # le port sur lequel la page est accessible (toujours 127.0.0.1:port)
    server.launch()


if __name__ == "__main__":

    mot= input("En quel mode lancer le programme? batch ou run?")
    if mot=="run":
        random.seed()
        run_single_server()

    if mot == "batch":
        n_usv = int(input("quels est le nombre d'usv souhaités?(entre 3 et 30)"))
        n_dechet=int(input("quel est le nombre de déchets souhaités (entre 3 et 20)?"))
        distance_com=int(input("quel est la distance de communication souhaitée (entre 0 et 850)?"))

        parametres={ "n_dechets":n_dechet, "n_usv":n_usv, "distance_max_communication":distance_com}
        resultat = mesa.batch_run(RecuperationPlastique, parameters=parametres, iterations=10, max_steps=1000, number_processes=1,data_collection_period=-1, display_progress=True)

        dataframe_resultat = pd.DataFrame(resultat)
        print(dataframe_resultat)


